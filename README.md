# Tareas CLM DevOPs

#### Reto 1. Dockerize la aplicación

 La aplicación se encuentra dockerizada a través del archivo *Dockerfile* ubicado en la carpeta *nodeapp/*. Solo se debe ejecutar el siguiente comando para construir la imagen: 
 ```sh
 docker build -t nodeapp .
 ```
#### Reto 2. Docker Compose

 A través del archivo *docker-compose.yml* ubicado en la ruta raíz del repositorio se encuentran dockerizadas la aplicación en nodejs y el nginx. Nginx está configurado para redirigir desde *http* a *https* y con autenticación básica para el endpoint */private*. Para iniciar las aplicaciones solo se debe ejecutar el comando: 
 ```sh
 docker-compose up -d 
 ```
Para el acceso al endpoint */private*, los datos son:

Usuario: *usuario1*
Contraseña: *password*

#### Reto 3. Probar la aplicación en cualquier sistema CI/CD

 El archivo *.gitlab-ci.yml* se encarga de hacer el CI/CD de la aplicación nodejs en Gitlab. Realiza el build, corre los test, crear una imagen y la sube a docker hub, bajo el nombre de *archibaldo/nodeapp*. Se puede descargar con el comando: 
 ```sh
 docker pull archibaldo/nodeapp:latest
 ```
 
#### Reto 4. Deploy en kubernetes
 
 Los objetos de Kubernetes (deployment, service y Horizontal Pod Autoscaler), se encuentran en el archivo *k8s.yaml*, ubicado en la carpeta *nodeapp/*. Para instalarlo en Kubernetes se debe ejecutar el comando: 
 ```sh
 kubectl apply -f k8s.yaml
 ```
 
#### Reto 5. Construir Chart en helm y manejar trafico http(s)

 En la carpeta *helm/clm-nginx* se encuentra el Chart de Helm configurado para crear un objeto ingress con Nginx en Kubernetes. Se ha establecido la redirección a *https* desde *http* y se ha configurado con autenticación básica el endpoint */private*. Previo a la ejecución de dicho archivo se debe crear el secret con los datos del basic auth. Para ello, se debe ejecutar lo siguiente (desde el directorio *helm/*): 
 ```sh
 kubectl create secret generic basic-auth --from-file=auth
 ```
 Realizado lo anterior, desde el directorio *helm/clm-nginx*, se debe correr el comando: 
```sh
 helm install clm-nginx . 
```
 
#### Reto 6. Terraform 

El archivo ubicado en la carpeta *terraform/*, llamado *main.tf* contiene las instrucciones para crear un role en Kubernetes (usando el provider de *minikube*), con accesos de solo lectura para los pods del namespaces Default. Para ejecutarlo simplemente se debe inicializar Terraform con el comando:
```sh
 terraform init
```
Y ejecutar, desde el directorio donde se encuentre el script *main.tf*, el comando:
```sh
terraform apply 
```

 
#### Reto 7. Automatiza el despliegue de los retos realizados

Para ejecutar el makefile ubicado en la ruta *make/*, se deben cumplir los siguientes prerrequisitos
en el servidor donde se ejecute el makefile:
 - make *(instalado)*
 - git *(instalado)*
 - docker *(Instalado)*
 - docker-compose *(Instalado)*
 - minikube *(Ejecutándose con el addons ingress habilitado)*
 - helm
 - terraform

Además, se debe editar el archivo hosts con la siguiente entrada:
```sh
IP_MINIKUBE nodeapp.minikube.local
```
donde IP_MINIKUBE se obtiene ejecutando el comando:
```sh
minikube ip
```
Si se cumplen todos los requisitos, entonces hay que descargar el repositorio con el comando:
```sh
git clone https://gitlab.com/archibaldodelacruz/reto-devops.git
```

Luego hay que situarse en el directorio *make/* y ejecutar:
```sh
make
```

Si todo se ha realizado con éxito, entonces se debería poder acceder a la aplicación nodejs en las direcciones:

*https://nodeapp.minikube.local/public*
y
*https://nodeapp.minikube.local/private*

Para el acceso al endpoint */private*, los datos son:

Usuario: *usuario1*
Contraseña: *password*

##### Para consultas o dudas contactarse con Víctor Merino al correo <victor.merino@archibaldo.cl>



   
   
