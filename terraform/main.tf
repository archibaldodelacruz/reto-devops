provider "kubernetes" {
  config_context_cluster   = "minikube"
}

resource "kubernetes_role" "SoloLecturaPodsNamespaceDefault" {
  metadata {
    name = "default-sololecturapods"
    namespace = "default"
  }
  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

